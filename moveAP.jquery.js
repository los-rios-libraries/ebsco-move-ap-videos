var apVideo = $('.apvideo', window.parent.document);
$('.result-list', window.parent.document).append(apVideo);
if ($('#multiSelectDocType input', window.parent.document).is(':checked'))
{
  if (!($('#_doc_type_ALL, #_doc_type_849NP', window.parent.document).is(':checked')))
  {
    apVideo.hide();
  }
}
if ($('#multiSelectDbFilterContent input', window.parent.document).is(':checked'))
{
  if (!($('#_db_filter__all_dbs_', window.parent.document).is(':checked')))
  {
    apVideo.hide();
  }
}