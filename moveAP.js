var apVideo = window.parent.document.querySelector('.apvideo');
if (apVideo)
{
  window.parent.document.querySelector('.result-list').appendChild(apVideo);
  // this part checks whether format or db limiters have been set
  var sourceTypeInputs = window.parent.document.querySelectorAll('#multiSelectDocType input:checked');
  var dbInputs = window.parent.document.querySelectorAll('#multiSelectDbFilterContent input:checked');
  var videoSource = window.parent.getElementById('_doc_type_849NP'); // for when Videos is actually selected by the user
  if (sourceTypeInputs.length > 0)
  {
    if (videoSource)
    {
      if (videoSource.checked !== true)
      {
        apVideo.style.display = 'none';
      }
    }
    else if (window.parent.document.getElementById('_doc_type_ALL').checked !== true)
    { // the all sources limiter is checked when no others are, so need to control for that
      apVideo.style.display = 'none';
    }
  }
  if (dbInputs.length > 0)
  {
    if (window.parent.document.getElementById('_db_filter__all_dbs_').checked !== true)
    { // the all databases limiter is checked when no others are, so need to control for that
      apVideo.style.display = 'none';
    }
  }
}